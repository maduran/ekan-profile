<?php

/**
 * @file
 * Hooks provided by the EKAN Datastore module.
 */

/**
 * Execute actions right after importing a resource to the datastore.
 *
 * @param \Drupal\ekan_datastore\Resource $resource
 *   Resource object.
 */
function hook_datastore_post_import(\Drupal\ekan_datastore\Resource $resource) {
  if ($resource->getId()) {
    // Load resource node and execute needed actions.
  }
}
