<?php

namespace Drupal\ekan_datastore;

use Drupal\ekan_core\Entity\EkanResourceEntity;
use Drupal\file\FileInterface;

/**
 * Wrapper object for a resource entity.
 *
 * This was ported from 7.x DKAN and remains for now.
 */
class Resource {

  /**
   * The id of the resource.
   *
   * @var int
   */
  private $id;

  /**
   * File path to the csv/record of the resource.
   *
   * @var string
   */
  private $filePath;

  /**
   * The file entity.
   *
   * @var \Drupal\file\Entity\File
   */
  private $file;

  /**
   * Resource constructor.
   */
  private function __construct($id, $file_path, $file) {
    $this->id = $id;
    $this->filePath = $file_path;
    $this->file = $file;
  }

  /**
   * Getter.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Getter.
   */
  public function getFilePath() {
    return $this->filePath;
  }

  /**
   * Return file entity.
   *
   * @return \Drupal\file\Entity\File
   *   The file entity.
   */
  public function getFile() {
    return $this->file;
  }

  /**
   * Create a resource from a Resource uuid.
   *
   * @param string $uuid
   *   An entity's uuid.
   *
   * @return Resource
   *   Resource.
   */
  public static function createFromEkanResourceUuid($uuid) {
    $id = self::getIdFromUuid($uuid);
    return self::createFromEkanResourceId($id);
  }

  /**
   * Create a resource from a Resource id.
   *
   * @param string $id
   *   A resource entity id.
   *
   * @return Resource
   *   Resource.
   */
  public static function createFromEkanResourceId($id) {
    if ($entity = EkanResourceEntity::load($id)) {
      return self::createFromEkanResource($entity);
    }
    throw new \Exception(t('Failed to load resource node.'));
  }

  /**
   * Create a resource from Ekan Resource.
   *
   * @param \Drupal\ekan_core\Entity\EkanResourceEntity $resource
   *   An EKAN resource.
   *
   * @return Resource
   *   Resource.
   *
   * @throws \Drupal\ekan_datastore\UnsupportedDatastoreResourceException
   */
  public static function createFromEkanResource(EkanResourceEntity $resource) {
    $id = $resource->id();
    $file_params = self::fileInfo($resource);
    return new self($id, ...$file_params);
  }

  /**
   * Gets nid using uuid.
   */
  private static function getIdFromUuid($uuid) {
    /** @var \Drupal\Core\Entity\EntityRepository $er */
    $er = \Drupal::service('entity.repository');
    $entity = $er->loadEntityByUuid('resource', $uuid);
    $id = $entity->id();
    if ($id) {
      return $id;
    }
    else {
      throw new \Exception(t("uuid !uuid not found.", ['!uuid' => $uuid]));
    }
  }

  /**
   * Get file info for a resource's file.
   *
   * Regardless of whether it was uploaded or a remote file.
   */
  private static function fileInfo(EkanResourceEntity $resource) : array {
    $file = $resource->get('upload')->entity ?: $resource->get('link_remote_file')->entity;
    if ($file instanceof FileInterface) {
      // e.g. "text/csv; charset=UTF-8" or "text/csv".
      $filemime = explode(";", $file->getMimeType())[0];

      if (!in_array($filemime, [
        "text/csv",
        "text/tsv",
        "text/tab-separated-values",
      ])) {
        error_log((new \Exception())->getTraceAsString());
        throw new UnsupportedDatastoreResourceException(t("This filemime type (:filemime) can be added as a resource, but cannot be imported to our datastore.", [':filemime' => $filemime]));
      }

      // Http(s) urls return as is.
      $scheme = parse_url($file->getFileUri(), PHP_URL_SCHEME);
      if (in_array($scheme, ['https', 'http'])) {
        @stream_wrapper_restore($scheme);
        return [$file->getFileUri(), $file];
      }

      // Return real file system path for public/private files.
      return [
        'file_path' => \Drupal::service('file_system')->realpath($file->getFileUri()),
        'file' => $file,
      ];
    }

    throw new \Exception(t("Resource :id doesn't have a proper file path.", [':nid' => $resource->id()]));
  }

}
