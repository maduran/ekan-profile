<?php

namespace Drupal\ekan_datastore;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\ekan_core\Entity\EkanResourceEntity;

/**
 * Computed field for showing the datastore import status.
 */
class ComputedStatusField extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritDoc}
   */
  protected function computeValue() {
    $resource = $this->getEntity();
    assert($resource instanceof EkanResourceEntity);
    $this->list[0] = $this->createItem(0, 2);
  }

}
