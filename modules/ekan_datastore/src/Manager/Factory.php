<?php

namespace Drupal\ekan_datastore\Manager;

use Drupal\ekan_datastore\LockableDrupalVariables;
use Drupal\ekan_datastore\Resource;

/**
 * Class Factory.
 *
 * Builds a datastore manager for a resource.
 */
class Factory {

  /**
   * A datastore resource wrapper.
   *
   * @var \Drupal\ekan_datastore\Resource
   */
  private $resource;

  /**
   * The Datastore Manager plugin ID.
   *
   * @var string
   */
  private $pluginId;

  /**
   * Constructor.
   */
  public function __construct(Resource $resource, $plugin_id = NULL) {
    $this->resource = $resource;
    $this->pluginId = $plugin_id;
  }

  /**
   * Get the datastore manager.
   */
  public function get() : DatastoreManagerInterface {

    /** @var \Drupal\Component\Plugin\PluginManagerInterface $datastore_manager_plugin_manager */
    $datastore_manager_plugin_manager = \Drupal::service('plugin.manager.datastore_manager');

    // If we know the plugin ID.
    $plugin_id = $this->pluginId ?: FALSE;

    // If the plugin id is from the state.
    if (!$plugin_id) {
      $state_storage = new LockableDrupalVariables("ekan_datastore");
      $state = $state_storage->get($this->resource->getId());
      $plugin_id = $state['plugin_id'] ?? FALSE;
    }

    // Fall back to the first one off the list.
    if (!$plugin_id) {
      $definitions = $datastore_manager_plugin_manager->getDefinitions();
      $plugin_id = key($definitions);
    }

    if ($plugin_id) {
      $manager = $datastore_manager_plugin_manager->createInstance($plugin_id, [
        'resource' => $this->resource,
      ]);
      assert($manager instanceof DatastoreManagerInterface);
      return $manager;
    }

    throw new \Exception("Unable to find a datastore manager plugin for this resource");

  }

}
