<?php

namespace Drupal\ekan_datastore\Manager;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Site\Settings;
use Drupal\ekan_datastore\DatastoreStorage;
use Drupal\ekan_datastore\LockableDrupalVariables;
use Drupal\ekan_datastore\Resource;
use League\Csv\Reader;
use League\Csv\TabularDataReader;

/**
 * Class Manager.
 */
abstract class DatastoreManagerBase extends PluginBase implements DatastoreManagerInterface {

  /**
   * List of errors.
   *
   * @var array
   */
  private $errors = [];

  /**
   * Current state of the import.
   *
   * @var int
   */
  protected int $stateDataImport;

  /**
   * Current state of the storage.
   *
   * @var int
   */
  protected int $stateStorage;

  /**
   * The available config properties.
   *
   * @var array
   */
  private $configurableProperties;

  /**
   * A datastore resourc wrapper object.
   *
   * @var \Drupal\ekan_datastore\Resource
   */
  private $resource;

  /**
   * The parser.
   *
   * @var \League\Csv\TabularDataReader
   */
  private $parser;

  /**
   * The number of seconds before giving up on an import via the UI.
   *
   * @var int
   */
  private int $timeLimit;

  /**
   * Constructor for a Datastore Manager plugin.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    $this->timeLimit = 0;

    $this->resource = $configuration['resource'];

    $this->stateDataImport = self::DATA_IMPORT_UNINITIALIZED;
    $this->stateStorage = self::STORAGE_UNINITIALIZED;

    $this->configurableProperties = [];

    if (!$this->loadState()) {
      $this->setConfigurablePropertiesHelper([
        'encoding' => CharsetEncoding::DEST_ENCODING,
        'delimiter' => ',',
        'quote' => '"',
        'escape' => '\\',
        'trailing_delimiter' => FALSE,
      ]);

      if ($configuration['resource']) {
        $this->initialization($configuration['resource']);
      }
    }

    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritDoc}
   */
  public function getImportType() {
    return 'PHP';
  }

  /**
   * {@inheritDoc}
   */
  public function setImportTimelimit($seconds) {
    if ($seconds > 0) {
      $this->timeLimit = $seconds;
    }
    else {
      $this->timeLimit = 0;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getResource() : Resource {
    return $this->resource;
  }

  /**
   * Get parser.
   *
   * @return \League\Csv\TabularDataReader
   *   Parser object.
   */
  protected function getParser() : TabularDataReader {
    if (!$this->parser) {
      $reader = Reader::createFromPath($this->resource->getFilePath(), 'r');
      $reader->setDelimiter($this->configurableProperties['delimiter']);
      $reader->setEscape($this->configurableProperties['escape']);
      $reader->setEnclosure($this->configurableProperties['quote']);
      $reader->setHeaderOffset(0);

      $input_bom = $reader->getInputBOM();
      $source_encoding = $this->getConfigurableProperties()['encoding']['PHP'] ?? NULL;
      if ($source_encoding != CharsetEncoding::DEST_ENCODING['PHP']) {
        $reader->addStreamFilter("convert.iconv.$source_encoding/UTF-8");
      }
      elseif ($input_bom === Reader::BOM_UTF16_LE || $input_bom === Reader::BOM_UTF16_BE) {
        $reader->addStreamFilter('convert.iconv.UTF-16/UTF-8');
      }

      $this->parser = $reader;
    }
    return $this->parser;
  }

  /**
   * Initialization.
   *
   * This method is called the first time an instance of a
   * Manager is created.
   *
   * This gives specific classes to affect what happens
   * during construction.
   *
   * @param \Drupal\ekan_datastore\Resource $resource
   *   Resource.
   */
  abstract protected function initialization(Resource $resource);

  /**
   * {@inheritdoc}
   */
  private function initializeStorage() {
    $table_name = $this->getTableName();

    if (!DatastoreStorage::database()->schema()->tableExists($table_name)) {
      $schema = $this->getTableSchema();

      DatastoreStorage::database()->schema()->createTable($table_name, $schema);

      $this->stateStorage = self::STORAGE_INITIALIZED;
      $this->saveState();
    }
    elseif ($this->stateStorage == self::STORAGE_UNINITIALIZED) {
      $this->stateStorage = self::STORAGE_INITIALIZED;
      $this->saveState();
    }
  }

  /**
   * Get table schema.
   */
  public function getTableSchema() {
    $schema = [];
    $header = $this->getTableHeaders();
    foreach ($header as $field) {
      $schema['fields'][$field] = [
        'label' => $field,
        'type' => "text",
      ];
    }

    $schema['fields']['entry_id'] = [
      'label' => 'entry_id',
      'type' => 'serial',
      'unsigned' => TRUE,
      'not null' => TRUE,
    ];
    $schema['primary key'] = [
      '0' => 'entry_id',
    ];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getTableHeaders(): array {
    $headers = $this->getParser()->getHeader();

    foreach ($headers as $key => $field) {
      $new = preg_replace("/[^A-Za-z0-9_ ]/", '', $field);
      // Fallback when columns could be empty.
      if ($new === '') {
        $new = "col_$key";
      }
      $new = self::safeName($new);
      $header[$key] = $new;
    }

    if (empty($header)) {
      throw new \Exception("Unable to get headers from {$this->resource->getFilePath()}");
    }

    return $header;
  }

  /**
   * Private method.
   */
  private function loadState() {
    $state_storage = new LockableDrupalVariables("ekan_datastore");
    if (!$this->resource) {
      return FALSE;
    }
    $state = $state_storage->get($this->resource->getId());

    if ($state) {
      if ($state['storage']) {
        $this->stateStorage = $state['storage'];
      }
      if ($state['data_import']) {
        $this->stateDataImport = $state['data_import'];
      }
      if ($state['configurable_properties']) {
        $this->setConfigurablePropertiesHelper($state['configurable_properties']);
      }
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function saveState() {
    $state_storage = new LockableDrupalVariables("ekan_datastore");
    $state_storage->set($this->resource->getId(), $this->getStatus());
  }

  /**
   * {@inheritdoc}
   */
  public function dropState() {
    $state_storage = new LockableDrupalVariables("ekan_datastore");
    $state_storage->delete($this->resource->getId());
    $this->stateStorage = self::STORAGE_UNINITIALIZED;
    $this->stateDataImport = self::DATA_IMPORT_UNINITIALIZED;
  }

  /**
   * {@inheritdoc}
   */
  public function import() {
    $status = $this->getStatus();
    if ($status['storage'] == self::STORAGE_UNINITIALIZED) {
      $this->initializeStorage();
    }

    $this->stateDataImport = self::DATA_IMPORT_IN_PROGRESS;
    $this->saveState();

    $import_state = $this->storeRecords($this->timeLimit);
    if ($import_state == self::DATA_IMPORT_DONE) {
      $this->stateDataImport = self::DATA_IMPORT_DONE;
      $this->saveState();
      \Drupal::moduleHandler()->invokeAll('datastore_post_import', [$this->resource]);
    }
    elseif ($import_state == self::DATA_IMPORT_PAUSED) {
      $this->stateDataImport = self::DATA_IMPORT_PAUSED;
      $this->saveState();
    }
    elseif ($import_state == self::DATA_IMPORT_ERROR) {
      $this->stateDataImport = self::DATA_IMPORT_ERROR;
      \Drupal::logger('ekan_datastore')->error(implode("\n", $this->errors));
      $this->saveState();
    }
    else {
      throw new \Exception("An incorrect state was returnd by storeRecords().");
    }

    return $import_state;
  }

  /**
   * Store records.
   *
   * Move records from the resource to the datastore.
   *
   * @return bool
   *   Whether the storing process was successful.
   */
  abstract protected function storeRecords($time_limit = 0);

  /**
   * {@inheritdoc}
   */
  public function drop() {
    $this->dropTable();
    $this->dropState();
  }

  /**
   * Drop table.
   */
  protected function dropTable() {
    DatastoreStorage::database()->schema()->dropTable($this->getTableName());
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    $state = [];

    $state['plugin_id'] = $this->getPluginId();
    $state['class'] = static::class;
    $state['storage'] = $this->stateStorage;
    $state['data_import'] = $this->stateDataImport;
    $state['configurable_properties'] = $this->getConfigurableProperties();

    return $state;
  }

  /**
   * {@inheritdoc}
   */
  public function getTableName() {
    $tablename_prefix = Settings::get('ekan_datastore_tablename_prefix', 'ekan_datastore_');
    return "{$tablename_prefix}{$this->resource->getId()}";
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigurableProperties() {
    return $this->configurableProperties;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfigurableProperties($properties) {
    $this->setConfigurablePropertiesHelper($properties);
    $this->stateDataImport = self::DATA_IMPORT_READY;
    $this->saveState();
  }

  /**
   * Helper.
   */
  private function setConfigurablePropertiesHelper($properties) {
    $this->configurableProperties = $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function numberOfRecordsImported() {
    $table_name = $this->getTableName();
    $query = DatastoreStorage::database()->select($table_name, "t");

    try {
      return $query->countQuery()->execute()->fetchField();
    }
    catch (\Exception $exception) {
      return 0;
    }
  }

  /**
   * Set error.
   *
   * Adds an error message to the errors array.
   */
  protected function setError($error) {
    $this->errors[] = $error;
  }

  /**
   * {@inheritdoc}
   */
  public function getErrors() {
    return $this->errors;
  }

  /**
   * {@inheritdoc}
   */
  public function goToPausedState() {
    $this->stateDataImport = $this::DATA_IMPORT_PAUSED;
    $this->saveState();
  }

  /**
   * {@inheritdoc}
   */
  public function getInterrupt() : int {
    return \Drupal::state()->get('ekan_datastore_interrupt', 0);
  }

  /**
   * {@inheritdoc}
   */
  public function setInterrupt(int $value = 1) : void {
    \Drupal::state()->set('ekan_datastore_interrupt', $value);
  }

  /**
   * {@inheritdoc}
   */
  public function safeName($name) : string {
    $map = [
      '.' => '_',
      ':' => '',
      '/' => '',
      '-' => '_',
      ' ' => '_',
      ',' => '_',
    ];
    $simple = trim(strtolower(strip_tags($name)));
    // Limit length to
    // 64 http://dev.mysql.com/doc/refman/5.0/en/identifiers.html
    $simple = substr(strtr($simple, $map), 0, 64);

    if (is_numeric($simple)) {
      // We need to escape numerics because Drupal's drupal_write_record()
      // does not properly escape token MYSQL names.
      $simple = '__num_' . $simple;
    }

    if (in_array($simple, self::reservedWords())) {
      $simple = $simple . '_';
    }

    return DatastoreStorage::database()->escapeTable($simple);
  }

  /**
   * {@inheritdoc}
   */
  public function reservedWords() : array {
    return [
      'accessible',
      'add',
      'all',
      'alter',
      'analyze',
      'and',
      'as',
      'asc',
      'asensitive',
      'before',
      'between',
      'bigint',
      'binary',
      'blob',
      'both',
      'by',
      'call',
      'cascade',
      'case',
      'change',
      'char',
      'character',
      'check',
      'collate',
      'column',
      'condition',
      'constraint',
      'continue',
      'convert',
      'create',
      'cross',
      'current_date',
      'current_time',
      'current_timestamp',
      'current_user',
      'cursor',
      'database',
      'databases',
      'day_hour',
      'day_microsecond',
      'day_minute',
      'day_second',
      'dec',
      'decimal',
      'declare',
      'default',
      'delayed',
      'delete',
      'desc',
      'describe',
      'deterministic',
      'distinct',
      'distinctrow',
      'div',
      'double',
      'drop',
      'dual',
      'each',
      'else',
      'elseif',
      'enclosed',
      'escaped',
      'exists',
      'exit',
      'explain',
      'false',
      'fetch',
      'float',
      'float4',
      'float8',
      'for',
      'force',
      'foreign',
      'from',
      'fulltext',
      'get',
      'grant',
      'group',
      'having',
      'high_priority',
      'hour_microsecond',
      'hour_minute',
      'hour_second',
      'if',
      'ignore',
      'in',
      'index',
      'infile',
      'inner',
      'inout',
      'insensitive',
      'insert',
      'int',
      'int1',
      'int2',
      'int3',
      'int4',
      'int8',
      'integer',
      'interval',
      'into',
      'io_after_gtids',
      'io_before_gtids',
      'is',
      'iterate',
      'join',
      'key',
      'keys',
      'kill',
      'leading',
      'leave',
      'left',
      'like',
      'limit',
      'linear',
      'lines',
      'load',
      'localtime',
      'localtimestamp',
      'lock',
      'long',
      'longblob',
      'longtext',
      'loop',
      'low_priority',
      'master_bind',
      'master_ssl_verify_server_cert',
      'match',
      'maxvalue',
      'mediumblob',
      'mediumint',
      'mediumtext',
      'middleint',
      'minute_microsecond',
      'minute_second',
      'mod',
      'modifies',
      'natural',
      'not',
      'no_write_to_binlog',
      'null',
      'numeric',
      'on',
      'optimize',
      'option',
      'optionally',
      'or',
      'order',
      'out',
      'outer',
      'outfile',
      'partition',
      'precision',
      'primary',
      'procedure',
      'purge',
      'range',
      'read',
      'reads',
      'read_write',
      'real',
      'references',
      'regexp',
      'release',
      'rename',
      'repeat',
      'replace',
      'require',
      'resignal',
      'restrict',
      'return',
      'revoke',
      'right',
      'rlike',
      'schema',
      'schemas',
      'second_microsecond',
      'select',
      'sensitive',
      'separator',
      'set',
      'show',
      'signal',
      'smallint',
      'spatial',
      'specific',
      'sql',
      'sqlexception',
      'sqlstate',
      'sqlwarning',
      'sql_big_result',
      'sql_calc_found_rows',
      'sql_small_result',
      'ssl',
      'starting',
      'straight_join',
      'table',
      'terminated',
      'then',
      'tinyblob',
      'tinyint',
      'tinytext',
      'to',
      'trailing',
      'trigger',
      'true',
      'undo',
      'union',
      'unique',
      'unlock',
      'unsigned',
      'update',
      'usage',
      'use',
      'using',
      'utc_date',
      'utc_time',
      'utc_timestamp',
      'values',
      'varbinary',
      'varchar',
      'varcharacter',
      'varying',
      'when',
      'where',
      'while',
      'with',
      'write',
      'xor',
      'year_month',
      'zerofill',
    ];
  }

}
