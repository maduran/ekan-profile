<?php

namespace Drupal\ekan_datastore\Manager;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin manager class for collecting data store manager plugins.
 */
class DatastoreManagerPluginManager extends DefaultPluginManager {

  /**
   * {@inheritDoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {

    parent::__construct(
      'Plugin/DatastoreManager',
      $namespaces,
      $module_handler,
      'Drupal\ekan_datastore\Manager\DatastoreManagerInterface',
    );
    $this->alterInfo('datastore_manager');
    $this->setCacheBackend($cache_backend, 'datastore_manager_plugins');
  }

}
