<?php

namespace Drupal\ekan_datastore\Manager;

use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\ekan_datastore\Resource;

/**
 * Interface ManagerInterface.
 */
interface DatastoreManagerInterface extends PluginInspectionInterface, DerivativeInspectionInterface {

  const STORAGE_UNINITIALIZED = 0;
  const STORAGE_INITIALIZED = 1;

  const DATA_IMPORT_UNINITIALIZED = 2;
  const DATA_IMPORT_READY = 3;
  const DATA_IMPORT_IN_PROGRESS = 4;
  const DATA_IMPORT_PAUSED = 5;
  const DATA_IMPORT_DONE = 6;
  const DATA_IMPORT_ERROR = 7;

  /**
   * Drops a datastore.
   */
  public function drop();

  /**
   * Imports data into a datastore.
   */
  public function import();

  /**
   * Set the time limit.
   *
   *  The import process will stop if it hits the time limit.
   *
   * @param int $seconds
   *   Number of seconds.
   */
  public function setImportTimelimit($seconds);

  /**
   * Get status.
   *
   * The status (state information) is meant to inform the
   * outside world of what the status of a given datastore is.
   *
   * @return array
   *   An array with the status (state) information.
   */
  public function getStatus();

  /**
   * Get Configurable properties.
   *
   * If a datastore requires information from the user, it
   * can make the system aware of those requirements by
   * returning an array from this method.
   *
   * @return array
   *   An associative array where the key is the property name
   *   and the value is the default value for the property.
   */
  public function getConfigurableProperties();

  /**
   * Setter.
   */
  public function setConfigurableProperties($properties);

  /**
   * The number of records in the datastore.
   */
  public function numberOfRecordsImported();

  /**
   * Get table name.
   *
   * The datastore mechanics are still tied to one type of storage:
   * Tables in the drupal database.
   *
   * @return string
   *   The name of that table.
   */
  public function getTableName();

  /**
   * Get table headers.
   *
   * @return array
   *   The names of the columns in the db table.
   */
  public function getTableHeaders();

  /**
   * Get errors.
   *
   * @return array
   *   An array of error messages (Strings).
   */
  public function getErrors();

  /**
   * Save state.
   *
   * Move the current state of the datastore manager to persistent storage.
   */
  public function saveState();

  /**
   * Drop state.
   *
   * Remove datastore state from persistant storage.
   */
  public function dropState();

  /**
   * Get table schema.
   */
  public function getTableSchema();

  /**
   * Change the datastore state to paused.
   */
  public function goToPausedState();

  /**
   * Get the type of import this is set up for.
   *
   * @return string
   *   'PHP' or 'MYSQL'
   */
  public function getImportType();

  /**
   * Get Interrupt flag.
   */
  public function getInterrupt() : int;

  /**
   * Set interrupt flag.
   */
  public function setInterrupt(int $value = 1) : void;

  /**
   * Determines safe name using reserved words.
   */
  public function safeName($name) : string;

  /**
   * Creates list of reserved words for the database.
   */
  public function reservedWords() : array;

  /**
   * Get resource.
   *
   * @return \Drupal\ekan_datastore\Resource
   *   The resource associated with this datastore.
   */
  public function getResource() : Resource;

}
