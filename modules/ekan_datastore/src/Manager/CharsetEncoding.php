<?php

namespace Drupal\ekan_datastore\Manager;

use Drupal\ekan_datastore\DatastoreStorage;
use Drupal\ekan_datastore\Resource;

/**
 * Class CharsetEncoding.
 *
 * Handles the encoding of data that is not in UTF-8.
 *
 * @package Drupal\ekan_datastore\Manager
 */
class CharsetEncoding {

  /**
   * Strings representing the destination character set.
   */
  const DEST_ENCODING = [
    'PHP' => 'UTF-8',
    'MYSQL' => 'utf8',
  ];

  /**
   * Data store manager.
   *
   * @var \Drupal\ekan_datastore\Manager\DatastoreManagerInterface
   * */
  private $manager;

  /**
   * Constructor for CharsetEncoding.
   */
  public function __construct($manager) {
    $this->manager = $manager;
  }

  /**
   * Gets a list of character sets in the selected naming convention.
   *
   * @param string $type
   *   The naming convention ('PHP' or 'MYSQL'.
   *
   * @return array|false|string[]
   *   Character set names.
   */
  public static function getEncodings($type) {
    $list = [];
    switch ($type) {
      case 'PHP':
        $list = mb_list_encodings();
        // Make the key/values the same in the array.
        $list = array_combine($list, $list);
        break;

      case 'MYSQL':
        $list = DatastoreStorage::database()->query('SELECT CHARACTER_SET_NAME, DESCRIPTION FROM INFORMATION_SCHEMA.CHARACTER_SETS')->fetchAllKeyed();
        break;
    }

    natsort($list);
    return $list;
  }

  /**
   * Select list(s) of encodings, appropriate to available importers.
   *
   * @todo Make Chosen update on #states change.
   * Everything works fine if only one import manager is enabled, but if
   * more than one is enabled, Chosen never loads values for the hidden
   * encoding select.
   *
   * @param array $default_value
   *   The default encoding to use.
   * @param \Drupal\ekan_datastore\Resource $resource
   *   A resource wrapper.
   *
   * @return array
   *   Form elements for select lists.
   */
  public static function getForm(array $default_value, Resource $resource) {
    /** @var \Drupal\Component\Plugin\PluginManagerInterface $datastore_manager_plugin_manager */
    $datastore_manager_plugin_manager = \Drupal::service('plugin.manager.datastore_manager');
    $form = [];

    foreach ($datastore_manager_plugin_manager->getDefinitions() as $manager_defn) {
      /** @var \Drupal\ekan_datastore\Manager\DatastoreManagerInterface $plugin */
      $plugin = $datastore_manager_plugin_manager->createInstance($manager_defn['id'], ['resource' => $resource]);

      $import_type = $plugin->getImportType();
      $select_name = strtolower("datastore_manager_config_encoding_$import_type");
      $form[$select_name] = [
        '#type' => 'select',
        // @codingStandardsIgnoreStart
        '#title' => t('Character encoding of file'),
        // @codingStandardsIgnoreEnd
        '#options' => self::getEncodings($import_type),
        '#default_value' => $default_value[$import_type],
      ];

      if (count($datastore_manager_plugin_manager->getDefinitions()) > 1) {
        $form[$select_name]['#states'] = [
          'visible' => [
            ':input[name="datastore_managers_selection"]' => [
              'value' => $manager_defn['label'],
            ],
          ],
        ];
      }
    }
    return $form;
  }

  /**
   * Ensures that a string has the correct encoding.
   *
   * @param string $data
   *   A sting in the source charset.
   *
   * @return string
   *   The data string in the destination charset.
   *
   * @throws \Exception
   */
  public function fixEncoding($data) {
    $properties = $this->manager->getConfigurableProperties();
    if (mb_check_encoding($data, $properties['encoding']['PHP'])) {
      if ($properties['encoding']['PHP'] !== self::DEST_ENCODING['PHP']) {
        // Convert encoding. The conversion is to UTF-8 by default to prevent
        // SQL errors.
        $data = mb_convert_encoding($data, self::DEST_ENCODING['PHP'], $properties['encoding']['PHP']);
      }
    }
    else {
      throw new \Exception(t('Source file is not in ":encoding" encoding.', [':encoding' => $properties['encoding']['PHP']]));
    }

    return $data;
  }

  /**
   * Is this form element part of the 'encoding' property?
   *
   * @param string $property_name
   *   The property name to test?
   *
   * @return bool
   *   True if it is an encoding property
   */
  public static function isEncodingProperty($property_name) {
    $prefix = 'encoding_';
    return strcmp(substr($property_name, 0, strlen($prefix)), $prefix) === 0;
  }

  /**
   * Adds or updates an encoding property value.
   *
   * @param array $configurable_properties
   *   The properties to update.
   * @param string $property_name
   *   The name of the form element.
   * @param mixed $value
   *   The value of the form element.
   */
  public static function setEncodingProperty(array &$configurable_properties, $property_name, $value) {
    if (preg_match('/encoding_(.*)/', $property_name, $matches)) {
      $type = strtoupper($matches[1]);
      $configurable_properties['encoding'][$type] = $value;
    }
    else {
      $msg = t(':name is not a valid encoding property name', [':name' => $property_name]);
      \Drupal::messenger()->addError($msg);
    }
  }

}
