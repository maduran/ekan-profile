<?php

namespace Drupal\ekan_datastore\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\ekan_core\Entity\EkanResourceEntity;
use Drupal\ekan_datastore\Manager\DatastoreManagerInterface;
use Drupal\ekan_datastore\Manager\Factory;
use Drupal\ekan_datastore\Manager\ManagerConfiguration;
use Drupal\ekan_datastore\Manager\ManagerSelection;
use Drupal\ekan_datastore\Manager\Status;
use Drupal\ekan_datastore\Resource;
use Drupal\ekan_datastore\UnsupportedDatastoreResourceException;

/**
 * Form class for the Manage Datastore tab on a resource.
 */
class ManageDatastoreForm extends FormBase {

  const BATCH_ITERATIONS = 1;
  const BATCH_TIME_LIMIT = 5;

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'manage-datastore-form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EkanResourceEntity $resource = NULL) {
    $form['#resource'] = $resource;

    $show_drop_form = $form_state->getTemporaryValue('show_drop_form');
    if ($show_drop_form) {
      $form += [
        '#title' => t('Are you sure you want to drop this datastore?'),
        'prefix' => [
          '#type' => 'html_tag',
          '#tag' => 'p',
          'content ' => [
            '#markup' => t('This operation will destroy the db table and all the data previously imported.'),
          ],
        ],
        'confirm' => [
          '#type' => 'submit',
          '#value' => t('Drop'),
          '#name' => 'drop',
          '#submit' => ['::dropSubmit'],
        ],
        'cancel' => [
          '#type' => 'submit',
          '#value' => t('Cancel'),
          '#name' => 'cancel',
        ],
      ];
      return $form;
    }

    $html = t('<p><h3>Datastore:</h3> Import data from a <strong>CSV</strong> or <strong>TSV</strong> file into a database table to make it accessible through an API.</p>
               <p class="data-explorer-help"><i class="fa fa-info-circle" aria-hidden="true"></i> <strong>Important</strong> Confirm that your column names adhere to the <a href="https://dev.mysql.com/doc/refman/8.0/en/identifiers.html" target="_blank">MySQL identifier specifications</a></p>');
    $form['help'] = [
      '#type' => 'item',
      '#markup' => $html,
    ];

    /** @var \Drupal\ekan_datastore\Manager\DatastoreManagerInterface $manager */
    $res = Resource::createFromEkanResource($resource);

    /** @var \Drupal\ekan_datastore\Manager\DatastoreManagerInterface $manager */
    try {
      $manager = (new Factory($res))->get();
    }
    catch (\Exception $ex) {
      \Drupal::messenger()->addWarning($ex->getMessage());
      return $form;
    }
    $form['#manager'] = $manager;

    $state = $manager->getStatus();
    $context = [
      'class' => $manager->getPluginDefinition()['label'],
      'records' => $manager->numberOfRecordsImported(),
      'import' => Status::datastoreStateToString($state['data_import']),
    ];

    $form['status'] = [
      '#type' => 'item',
      '#title' => t('Datastore Status'),
      'info' => [
        '#type' => 'inline_template',
        '#template' => <<< TWIG
<dl class="datastore-status">
  <dt>{{ "Importer"|t }}</dt><dd>{{ class }}</dd>
  <dt>{{ "Records Imported"|t }}</dt><dd>{{ records }}</dd>
  <dt>{{ "Data Importing"|t }}</dt><dd>{{ import }}</dd>
</dl>
TWIG,
        '#context' => $context,
      ],
    ];

    $state = $manager->getStatus()['data_import'];
    if ($state == DatastoreManagerInterface::DATA_IMPORT_PAUSED) {
      \Drupal::messenger()
        ->addStatus(t("The datastore importer is currently paused. It will resume in the background the next time cron runs from drush."));
    }

    $status = $manager->getStatus();
    if (in_array($status['data_import'], [
      DatastoreManagerInterface::DATA_IMPORT_READY,
      DatastoreManagerInterface::DATA_IMPORT_UNINITIALIZED,
    ])) {

      $form += (new ManagerSelection($res, $manager))->getForm();

      $form += (new ManagerConfiguration($manager))->getForm();

      $form['actions'] = ['#type' => 'actions'];
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => t("Import"),
        '#name' => 'import',
        '#submit' => ['::importSubmit'],
      ];
    }
    elseif (in_array($status['data_import'],
      [
        DatastoreManagerInterface::DATA_IMPORT_DONE,
        DatastoreManagerInterface::DATA_IMPORT_PAUSED,
        DatastoreManagerInterface::DATA_IMPORT_ERROR,
      ])) {

      $form['drop'] = [
        'confirm' => [
          '#type' => 'submit',
          '#value' => t('Drop'),
          '#name' => 'confirm_drop',
          '#submit' => ['::dropConform'],
        ],
      ];
    }
    elseif (in_array($status['data_import'], [DatastoreManagerInterface::DATA_IMPORT_IN_PROGRESS])) {
      $form['actions']['stop'] = [
        '#type' => 'submit',
        '#value' => t("Stop"),
        '#submit' => ['::stopSubmit'],
      ];

      $form['actions']['advanced'] = [
        '#type' => 'fieldset',
        '#title' => t('Advanced'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      ];

      $form['actions']['advanced']['help'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        'content ' => [
          '#markup' => t('When a datastore import shows an "in-progress" state but is stalled (no active cron job is importing new records into the datastore), the "Go to Paused State" button will return the datastore import to the paused state. It will then continue to be processed in the next cron run. <em>Use this option with caution</em>, as it will cause problems with the datastore if used in any scenario other than the one described above.'),
        ],
      ];

      $form['actions']['advanced']['pause'] = [
        '#type' => 'submit',
        '#value' => t("Go to Paused State"),
        '#submit' => ['::goToPausedSubmit'],
      ];
    }
    return $form;
  }

  /**
   * Submit handler for stopping an import.
   */
  public function stopSubmit(array &$form, FormStateInterface $form_state) {
    \Drupal::messenger()->addStatus(t("Importing will be stopped shortly."));
    \Drupal::state()->set('ekan_datastore_interrupt', 1);
  }

  /**
   * Submit handler for pausing an import.
   */
  public function goToPausedSubmit($form, FormStateInterface &$form_state) {
    $resource = $form['#resource'];
    try {
      /** @var \Drupal\ekan_datastore\Manager\DatastoreManagerInterface $manager */
      $manager = (new Factory(Resource::createFromEkanResource($resource)))->get();
      $manager->goToPausedState();
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }
  }

  /**
   * Submit handler.
   */
  public function dropConform($form, FormStateInterface &$form_state) {
    $form_state->setTemporaryValue('show_drop_form', TRUE);
    $form_state->setRebuild();
  }

  /**
   * Submit handler.
   */
  public function dropSubmit($form, FormStateInterface &$form_state) {
    $form_state->setTemporaryValue('show_drop_form', NULL);
    $manager = $this->getSelectedManager($form, $form_state);
    $manager->drop();
    $resource = $form['#resource'];
    assert($resource instanceof EkanResourceEntity);
    \Drupal::messenger()->addStatus(t("The datastore for %title has been successfully dropped.", ['%title' => $resource->label()]));
    $form_state->setRebuild();
  }

  /**
   * Gets the currently selected datastore manager plugin.
   */
  private function getSelectedManager(array &$form, FormStateInterface $form_state) : DatastoreManagerInterface {
    assert($form['#resource'] instanceof EkanResourceEntity);
    $selected_manager = $form_state->getValue('datastore_managers_selection');
    $res = Resource::createFromEkanResource($form['#resource']);
    return (new Factory($res, $selected_manager))->get();
  }

  /**
   * Submit handler for importing the datastore.
   */
  public function importSubmit(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\ekan_datastore\Manager\DatastoreManagerInterface $manager */
    $manager = $this->getSelectedManager($form, $form_state);

    try {
      $manager_values = [];
      foreach ($form_state->getValues() as $property_name => $v) {
        if (substr_count($property_name, "datastore_manager_config") > 0) {
          $manager_values[$property_name] = $v;
        }
      }
      if (!empty($manager_values)) {
        (new ManagerConfiguration($manager))->submit($manager_values);
      }

      $op = $form_state->getTriggeringElement()['#name'];
      if ($op == 'import') {
        $this->batchConfiguration($form['#resource'], $manager);
      }
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setTemporaryValue('show_drop_form', NULL);
  }

  /**
   * Batch event handler.
   */
  public static function batchProcess($resource_id, $manager_plugin_id, &$context) {
    @stream_wrapper_restore("https");
    @stream_wrapper_restore("http");
    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = 1;
    }

    $ekan_resource = EkanResourceEntity::load($resource_id);
    $res = Resource::createFromEkanResource($ekan_resource);

    try {

      $manager = (new Factory($res, $manager_plugin_id))->get();
      $manager->setImportTimelimit(self::BATCH_TIME_LIMIT);

      /** @var \Drupal\ekan_datastore\Manager\DatastoreManagerInterface $manager */
      $finished = $manager->import();
      if ($finished == DatastoreManagerInterface::DATA_IMPORT_ERROR) {
        $general = "EKAN DATASTORE: There was a problem while importing the Resource";
        $errors = $manager->getErrors();
        $error_string = implode(" | ", $errors);
        $final_error_string = "{$general} - {$error_string}";
        \Drupal::messenger()->addError($final_error_string);
      }
    }
    catch (\Exception $e) {
      $context['sandbox']['progress'] = 1;
      \Drupal::messenger()->addError($e->getMessage());
    }

    if ($finished == DatastoreManagerInterface::DATA_IMPORT_PAUSED) {
      return FALSE;
    }

    $context['sandbox']['progress']++;

    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Batch event handler.
   */
  public static function batchFinished($success, $results, $operations) {
  }

  /**
   * Setting up the batch process for importing a file.
   */
  private function batchConfiguration(EkanResourceEntity $resource, DatastoreManagerInterface $manager) {

    $batch_builder = (new BatchBuilder())
      ->setTitle(t('Importing.'))
      ->setFinishCallback(self::class . '::batchFinished');

    for ($i = 0; $i < self::BATCH_ITERATIONS; $i++) {
      $batch_builder->addOperation(self::class . '::batchProcess', [
        $resource->id(),
        $manager->getPluginId(),
      ]);
    }

    batch_set($batch_builder->toArray());
  }

  /**
   * Access callback.
   */
  public function manageDatastoreAccess(EkanResourceEntity $resource, AccountInterface $account): AccessResultInterface {
    try {
      $wrapper = Resource::createFromEkanResource($resource);
      return AccessResult::allowedIfHasPermission($account, 'manage datastore');
    }
    catch (UnsupportedDatastoreResourceException $e) {
      // Don't render or view link for this resource.
      return AccessResult::forbidden($e->getMessage());
    }
  }

}
