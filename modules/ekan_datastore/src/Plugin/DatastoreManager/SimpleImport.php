<?php

namespace Drupal\ekan_datastore\Plugin\DatastoreManager;

use Drupal\ekan_datastore\DatastoreStorage;
use Drupal\ekan_datastore\Manager\DatastoreManagerBase;
use Drupal\ekan_datastore\Manager\DatastoreManagerInterface;
use Drupal\ekan_datastore\Resource;
use League\Csv\Statement;

/**
 * Plugin implementation of the 'simple_import' datastore manager.
 *
 * @Plugin(
 *   id = "simple_import",
 *   label = "Simple Import"
 * )
 */
class SimpleImport extends DatastoreManagerBase implements DatastoreManagerInterface {

  /**
   * {@inheritdoc}
   */
  protected function initialization(Resource $resource) {}

  /**
   * {@inheritdoc}
   */
  protected function storeRecords($time_limit = 0) {
    $end = $time_limit ? time() + $time_limit : NULL;

    $number_of_items_imported = $this->numberOfRecordsImported();
    $start = ($number_of_items_imported > 0) ? $number_of_items_imported + 1 : 1;

    $base_query = DatastoreStorage::database()->insert($this->getTableName());
    $header = $this->getTableHeaders();
    $base_query->fields($header);

    $counter = 0;

    $reader = $this->getParser();

    try {
      $stmt = (new Statement())->offset($start);
      $records = $stmt->process($reader);
      $query = clone $base_query;
      // Insert 1000 at a time.
      foreach ($records as $record) {
        $query->values(array_values($record));
        $counter++;
        if ($counter % 1000 == 0) {
          $query->execute();
          $query = clone $base_query;
        }

        // If we hit the time limit, then pause import.
        if ($end && time() >= $end) {
          return DatastoreManagerInterface::DATA_IMPORT_PAUSED;
        }

        if ($this->getInterrupt()) {
          $this->setInterrupt(0);
          return DatastoreManagerInterface::DATA_IMPORT_PAUSED;
        }
      }

      // If we didn't make it to 1000.
      if ($counter % 1000 != 0) {
        $query->execute();
      }
    }
    catch (\Exception $e) {
      $this->setError($e->getMessage());
      return DatastoreManagerInterface::DATA_IMPORT_ERROR;
    }

    return DatastoreManagerInterface::DATA_IMPORT_DONE;
  }

}
