<?php

namespace Drupal\ekan_datastore\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\ekan_datastore\ComputedStatusField;
use Drupal\ekan_datastore\Manager\Factory;
use Drupal\ekan_datastore\Manager\Status;
use Drupal\ekan_datastore\Resource;
use Drupal\ekan_core\Entity\EkanResourceEntity;

/**
 * Plugin implementation of the 'datastore_status' formatter.
 *
 * User to format the \Drupal\ekan_datastore\ComputedStatusField values.
 *
 * @FieldFormatter(
 *   id = "datastore_status",
 *   label = @Translation("Datastore status"),
 *   field_types = {
 *     "integer"
 *   }
 * )
 */
class DatastoreStatus extends FormatterBase {

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $entity = $items->getEntity();
    assert($entity instanceof EkanResourceEntity);
    try {
      $resource = Resource::createFromEkanResource($entity);
      $manager = (new Factory($resource))->get();
      $status = $manager->getStatus();
      $string = Status::datastoreStateToString($status['data_import']);
      return [['#markup' => '<span class="circle true">' . $string . '</span>']];
    }
    catch (\Exception $e) {
      return [['#markup' => '<span class="circle true">' . t('Datastore disabled') . '</span>']];
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    if ($field_definition->getClass() == ComputedStatusField::class) {
      return TRUE;
    }
    $reflection = new \ReflectionClass($field_definition->getClass());
    return $reflection->isSubclassOf(ComputedStatusField::class);
  }

}
