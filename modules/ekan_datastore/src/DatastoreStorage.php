<?php

namespace Drupal\ekan_datastore;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Core\Site\Settings;

/**
 * A database connection to the datastore DB.
 *
 * To switch this to a different database, add a database config to
 * settings.php.
 *
 * e.g.
 * $databases['datastore']['default'] = [
 * 'driver' => 'mysql',
 * 'database' => 'datastore',
 * 'username' => 'data_user',
 * 'password' => 'data_user',
 * 'host' => 'mariadb',
 * ];
 *
 * Then tell this module to use that setting in settings.php.
 *
 * e.g.
 * $settings['ekan_datastore']['db_key'] = 'datastore';
 */
class DatastoreStorage {

  /**
   * Get the database used to store datastore tables.
   */
  public static function database() : Connection {
    $datastore_db_key = Settings::get('ekan_datastore', [])['db_key'] ?? 'default';
    $info = Database::getConnectionInfo($datastore_db_key);
    if (empty($info)) {
      throw new \Exception("No datastore database key defined");
    }

    $db = Database::getConnection('default', $datastore_db_key);
    if (!($db instanceof Connection)) {
      throw new \Exception("Problem connecting to datastore connection");
    }

    return $db;
  }

}
