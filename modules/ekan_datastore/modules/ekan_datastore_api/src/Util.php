<?php

namespace Drupal\ekan_datastore_api;

use Drupal\Component\Utility\NestedArray;

/**
 * Utility functions for datastore API.
 */
class Util {

  /**
   * Returns true if all the keys in a given array are strings.
   */
  public static function arrayIsAssoc($array) : bool {
    return is_array($array) && count(array_filter(array_keys($array), 'is_string')) > 0;
  }

  /**
   * Groups qualified fields.
   *
   * Given an array of qualified field returns
   * a new array with non qualified fields grouped
   * by table names.
   */
  public static function prepareFields($fields) {
    $grouped = [];
    foreach ($fields as $field) {
      [$table, $field] = explode('.', $field);
      $grouped[$table][] = $field;
    }
    return $grouped;
  }

  /**
   * Returns an array with fields, types and labels.
   */
  public static function schemaFields($resource_ids) {
    $fields = [];

    foreach ((array) $resource_ids as $id) {
      $schema = ekan_datastore_api_get_schema($id);
      $meta = ['fields' => []];

      $meta_fields = NestedArray::mergeDeep($meta['fields'], $schema['fields']);
      $fields = NestedArray::mergeDeep($fields, $meta_fields);
    }

    // Remove flatstore processor fields.
    // This is feeds specific, It should not be here.
    unset($fields['timestamp']);
    unset($fields['feeds_entity_id']);
    unset($fields['feeds_flatstore_entry_id']);
    return $fields;
  }

  /**
   * Given all the possible fields input returns a normalized version.
   *
   * Inputs:
   *
   * {fields: ['field']}
   * {fields: 'field,field2'}
   * {fields: {table:'field,field2'}}
   * {fields: {table:['field','field2']}}
   * {fields: {table: '*'}}
   *
   * Output:
   *
   * ['table_alias1.field1', 'table_alias1.field2']
   */
  public static function normalizeFields($fields, $resources) {
    $resources = is_string($resources) ? [EKAN_DATASTORE_API_DEFAULT_TABLE_ALIAS => $resources] : $resources;
    $new_fields = [];
    $normalized = [];
    foreach ($resources as $alias => $id) {

      if ((is_string($fields) && $fields == '*') || (self::arrayIsAssoc($fields) && isset($fields[$alias])) || count($resources) == 1) {
        // Fields comes with table prefix and fields are not specified.
        if (self::arrayIsAssoc($fields) && is_string($fields[$alias]) && $fields[$alias] == '*') {
          $schema = ekan_datastore_api_get_schema($id);

          $new_fields = self::buildQualifiedFields(array_keys($schema['fields']), $alias);

          // Fields comes with table prefix either using an
          // array or a string to set the fields.
        }
        elseif (self::arrayIsAssoc($fields)) {
          $new_fields = $fields[$alias];
          $new_fields = is_string($new_fields) ? explode(',', $new_fields) : $new_fields;
          $new_fields = self::buildQualifiedFields($new_fields, $alias);

          // Fields comes without table prefix within an array.
          // Since fields aren't prefixed we infer there is only
          // one alias.
        }
        elseif (is_array($fields)) {
          $new_fields = self::buildQualifiedFields($fields, $alias);

          // Fields are not specified so we get all of them
          // from the table schema.
        }
        elseif (is_string($fields) && $fields == '*') {
          $schema = ekan_datastore_api_get_schema($id);
          $new_fields = self::buildQualifiedFields(array_keys($schema['fields']), $alias);

          // Fields comes without table prefix and split by commas.
          // Since fields aren't prefixed we infer there is only
          // one alias.
        }
        elseif (is_string($fields)) {
          $new_fields = explode(',', $fields);
          $new_fields = self::buildQualifiedFields($new_fields, $alias);
        }
        $normalized = array_merge($normalized, $new_fields);
      }
    }
    return array_unique($normalized);
  }

  /**
   * Field safe name.
   *
   * It depends on the feeds_flatstore_processor_safe_name.
   * I decided to wrap it to make easy to decouple it.
   */
  public static function normalizeFieldName($field) {
    return $field;
  }

  /**
   * Create a qualified version of a field name.
   *
   * Qualified field names are those with the table prefix.
   * For example: table_name.field_name.
   */
  public static function buildQualifiedFields($fields, $alias) {
    return array_reduce((array) $fields, function ($memo, $field) use ($alias) {
      if (!ekan_datastore_api_field_excluded($field)) {

        $memo[] = $alias . '.' . self::normalizeFieldName($field);
      }
      return $memo;
    }, []);
  }

}
