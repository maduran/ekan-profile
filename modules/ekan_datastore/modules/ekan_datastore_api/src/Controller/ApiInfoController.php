<?php

namespace Drupal\ekan_datastore_api\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\ekan_datastore\Manager\Factory;
use Drupal\ekan_datastore\Resource;
use Drupal\ekan_core\Entity\EkanResourceEntity;
use Drupal\ekan_datastore\UnsupportedDatastoreResourceException;

/**
 * Controller to show the Data API tab on a resource.
 */
class ApiInfoController extends ControllerBase {

  /**
   * Output for datastore API instructions.
   */
  public function render(EkanResourceEntity $resource) {
    $output = '<h2>' . t('EKAN Datastore API') . '</h2>';

    $url = Url::fromRoute('rest.ekan_datastore_search.GET', [
      'resource_id' => $resource->uuid(),
      'limit' => 5,
    ])->setAbsolute();

    $output .= t('Access resource data via a web API with powerful query support.');
    $output .= '<h3>' . t('Resource ID') . '</h3>';
    $output .= t("The Resource ID for this resource is %id", ['%id' => $resource->uuid()]);
    $output .= '<h3>' . t('Example Query') . '</h3>';
    $output .= '<p>' . Link::fromTextAndUrl($url->toString(), $url)->toString() . '</p>';
    $output .= '<p>' . t('Query this datastore and return first five results') . '</p>';
    $output .= '<h3>' . t('Documentation') . '</h3>';
    $output .= '<p>' . t('See DKAN API documentation for more details: <a href="https://dkan.readthedocs.io/en/7.x-1.x/apis/datastore-api.html">DKAN Datastore API</a>') . '</p>';

    return [
      '#markup' => Markup::create($output),
    ];
  }

  /**
   * Access callback.
   */
  public function apiInfoAccess(EkanResourceEntity $resource, AccountInterface $account) {
    try {
      $wrapper = Resource::createFromEkanResource($resource);
    }
    catch (UnsupportedDatastoreResourceException $e) {
      // Don't render or view link for this resource.
      return AccessResult::forbidden();
    }

    if ($manager = (new Factory($wrapper))->get()) {
      $status = $manager->getStatus();
      if ($status['data_import'] != $manager::DATA_IMPORT_DONE) {
        return AccessResult::forbidden('Data import is not complete for this resource');
      }
    }
    return $resource->access('view', $account, TRUE);
  }

}
