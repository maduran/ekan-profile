<?php

namespace Drupal\ekan_datastore_api\Plugin\rest\resource;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\rest\ResourceResponseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Rest resource for a datastore imported from a resource.
 *
 * @RestResource(
 *   id = "ekan_datastore_search",
 *   label = @Translation("Search a EKAN Datastore"),
 *   uri_paths = {
 *     "canonical" = "/api/action/datastore/search.json",
 *     "create" = "/api/action/datastore/search.json"
 *   }
 * )
 */
class DatastoreSearch extends ResourceBase {

  /**
   * The entity repository used for uuid lookups.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  private $entityRepository;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityRepository = $container->get('entity.repository');
    return $instance;
  }

  /**
   * Responds to GET requests.
   *
   * Returns a rows from the datastore for the given params.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing the log entry.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   Thrown when the log entry was not found.
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   *   Thrown when no log entry was provided.
   */
  public function get(Request $request) : ResourceResponseInterface {
    $uuids = (array) $request->get('resource_id');

    if (empty($uuids)) {
      throw new BadRequestHttpException('No resource UUID was provided');
    }

    $cacheability = new CacheableMetadata();
    $cacheability->addCacheContexts(['url.query_args']);

    foreach ($uuids as $uuid) {
      $resource = $this->entityRepository->loadEntityByUuid('resource', $uuid);
      if (empty($resource)) {
        throw new NotFoundHttpException("Resource with UUID '$uuid' was not found");
      }
      $cacheability->addCacheableDependency($resource);
    }

    try {
      $params = ekan_datastore_api_get_params($request->query->all());
      $results = ekan_datastore_api_query($params);
      return (new ResourceResponse($results))->addCacheableDependency($cacheability);
    }
    catch (\Exception $ex) {
      throw new BadRequestHttpException($ex->getMessage());
    }
  }

  /**
   * Responds to POST requests.
   */
  public function post(Request $request) : ResourceResponseInterface {

    // POST requests are not normally cacheable by design.
    // So we enforce it here just to match up with what the old dkan system
    // used to do.
    $cid = md5($request->getContent());
    $cache_bin = \Drupal::cache('ekan_datastore_api');
    if ($cache = $cache_bin->get($cid)) {
      return (new ResourceResponse($cache->data));
    }

    $queries = json_decode($request->getContent(), TRUE);
    $cacheability = new CacheableMetadata();
    $results = [];
    foreach ((array) $queries as $name => $query) {
      foreach ($query['resource_id'] as $uuid) {
        $resource = $this->entityRepository->loadEntityByUuid('resource', $uuid);
        if (empty($resource)) {
          throw new NotFoundHttpException("Resource with UUID '$uuid' was not found");
        }
        $cacheability->addCacheableDependency($resource);
      }

      try {
        $params = ekan_datastore_api_get_params($query);
        $results[$name] = ekan_datastore_api_query($params);
      }
      catch (\Exception $ex) {
        throw new BadRequestHttpException($ex->getMessage());
      }
    }

    $cache_bin->set($cid, $results, Cache::PERMANENT, $cacheability->getCacheTags());

    return (new ResourceResponse($results));
  }

}
