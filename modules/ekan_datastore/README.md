# EKAN Datastore

This is a direct port of the original `dkan_datastore` module from DKAN for 
drupal 7.x.

Data store managers have been replaced by plugins, See ./src/Plugin/DatastoreManager/SimpleImport.php for an 
example implementation.

The "Fast Import" manager has not been ported.