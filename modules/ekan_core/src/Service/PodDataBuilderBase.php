<?php

namespace Drupal\ekan_core\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\ekan_core\Entity\EkanDatasetEntity;

/**
 * Base class for Project Open Data builder services.
 */
abstract class PodDataBuilderBase implements PodDataBuilderInterface {

  /**
   * The file system used for reading and writing caches.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;


  /**
   * The key value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValueStore;

  /**
   * The constructor.
   */
  public function __construct(FileSystemInterface $file_system, KeyValueFactoryInterface $key_value) {
    $this->fileSystem = $file_system;
    $this->keyValueStore = $key_value->get('ekan_pod_data');
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheFolder() {
    return 'public://pod_data';
  }

  /**
   * Dataset files folder.
   */
  public function getDatasetsFolder() {
    return 'public://pod_datasets';
  }

  /**
   * {@inheritDoc}
   */
  public function getCachedPath() {
    $cache_folder = $this->getCacheFolder();
    return "{$cache_folder}/{$this->getFilenamePrefix()}.{$this->getFileType()}";
  }

  /**
   * Get path of individual dataset file.
   */
  public function getDatasetFilePath($dataset_id) {
    $datasets_folder = $this->getDatasetsFolder();
    return "{$datasets_folder}/pod_dataset_{$dataset_id}.json";
  }

  /**
   * {@inheritDoc}
   */
  public function isCacheStale() {
    if (!is_file($this->getCachedPath())) {
      return TRUE;
    }

    if ($this->keyValueStore->get('last_build') + $this->cacheableMetadata()->getCacheMaxAge() <= \Drupal::time()->getCurrentTime()) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Warm the file data cache.
   *
   * @return string|null
   *   The filename of the generated file or NULL if something went wrong.
   */
  public function warmCache() {
    $cached_filepath = $this->getCachedPath();
    $cache_folder = dirname($cached_filepath);
    $this->fileSystem->prepareDirectory($cache_folder, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $data = $this->serializeDatasets($this->build($this->getDatasetEntityIds()));

    if ($this->fileSystem->saveData($data, $cached_filepath, FileSystemInterface::EXISTS_REPLACE)) {
      $this->keyValueStore->set('last_build', \Drupal::time()->getCurrentTime());
      Cache::invalidateTags(['pod_data_json']);
      return $cached_filepath;
    }

    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getDatasetEntityIds() : array {
    $ids = \Drupal::entityQuery('dataset')
      ->condition('status', 1)
      ->accessCheck(FALSE)
      ->execute();
    return $ids;
  }

  /**
   * {@inheritDoc}
   */
  public function buildDatasets(array $dataset_ids = []) : array {

    $datasets = [];
    foreach ($dataset_ids as $dataset_id) {
      $dataset_filepath = $this->getDatasetFilePath($dataset_id);

      if (!file_exists($dataset_filepath)) {
        $dataset = EkanDatasetEntity::load($dataset_id);
        $this->createDatasetJsonFile($dataset);
      }

      if (file_exists($dataset_filepath)) {
        $datasets[] = json_decode(file_get_contents($dataset_filepath), TRUE);
      }
    }

    return $datasets;
  }

  /**
   * Creates a dataset file. This gets created on every dataset entity save.
   */
  public function createDatasetJsonFile(EkanDatasetEntity $dataset) {
    $dataset_path = $this->getDatasetFilePath($dataset->id());
    $dataset_folder = dirname($dataset_path);
    $directory_writable = $this->fileSystem->prepareDirectory($dataset_folder, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    if ($directory_writable) {
      $data = Json::encode($this->buildDataset($dataset));
      if (file_exists($dataset_path)) {
        $this->fileSystem->delete($dataset_path);
      }
      return $this->fileSystem->saveData($data, $dataset_path, FileSystemInterface::EXISTS_REPLACE);
    }
    else {
      throw new \Exception("Directory $dataset_path is not writable");
    }
  }

  /**
   * {@inheritDoc}
   */
  public function cacheableMetadata() {
    $cachable_metadata = new CacheableMetadata();
    $cachable_metadata->setCacheTags([
      'dataset_list',
      'resource_list',
      'pod_data_json',
    ]);
    $cachable_metadata->setCacheMaxAge(24 * 60 * 60);
    return $cachable_metadata;
  }

}
