<?php

namespace Drupal\ekan_core\Controller;

use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\ekan_core\Service\PodDataBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller to build Project Open Data files (data.json, catalog.xml).
 */
interface PodDataApiInterface {

  /**
   * Get the service which will be used to build the json/xml.
   */
  public static function getPodDataBuilder(ContainerInterface $container) : PodDataBuilderInterface;

  /**
   * Return the dataset dcat catalog.xml redirect.
   *
   * @return \Drupal\Core\Routing\LocalRedirectResponse
   *   A redirect to the cached catalog.xml file.
   */
  public function retrieveAll(): LocalRedirectResponse;

}
