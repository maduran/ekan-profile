<?php

namespace Drupal\ekan_core\Controller;

use Drupal\ekan_core\Service\PodDataBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller to build Project Open Data data.json for Ekan datasets.
 */
class PodDatasetApi extends PodDataApiBase {

  /**
   * {@inheritDoc}
   */
  public static function getPodDataBuilder(ContainerInterface $container): PodDataBuilderInterface {
    return $container->get('ekan_core.pod_data_builder');
  }

}
