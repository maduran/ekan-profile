<?php

namespace Drupal\ekan_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\LocalRedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller to build Project Open Data files (data.json, catalog.xml).
 */
abstract class PodDataApiBase extends ControllerBase implements PodDataApiInterface {

  /**
   * The service which is generating our structured output.
   *
   * @var \Drupal\ekan_core\Service\PodDataBuilderInterface
   */
  public $datasetBuilder;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->datasetBuilder = $instance->getPodDataBuilder($container);
    return $instance;
  }

  /**
   * Return the dataset dcat catalog.xml redirect.
   *
   * @return \Drupal\Core\Routing\LocalRedirectResponse
   *   A redirect to the cached catalog.xml file.
   */
  public function retrieveAll(): LocalRedirectResponse {
    $file_path = $this->datasetBuilder->getCachedPath();
    $cachable_metadata = $this->datasetBuilder->cacheableMetadata();
    if (is_file($file_path)) {
      // Redirect should be cached for as long as the data is cached.
      $response = new LocalRedirectResponse(\Drupal::service('file_url_generator')->generateAbsoluteString($file_path));
      $response->getCacheableMetadata()->addCacheableDependency($cachable_metadata);
      $cache_expiry_time = \Drupal::time()->getRequestTime() + $cachable_metadata->getCacheMaxAge();
      $response->setExpires(\DateTime::createFromFormat('U', $cache_expiry_time));
      return $response;
    }
    else {
      throw new NotFoundHttpException();
    }
  }

}
