<?php

namespace Drupal\ekan_core\Controller;

use Drupal\ekan_core\Entity\EkanDatasetEntity;
use Drupal\ekan_core\Service\PodDataBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Controller to build Project Open Data catalog.xml for Ekan datasets.
 */
class DcatCatalogApi extends PodDataApiBase {

  /**
   * {@inheritDoc}
   */
  public static function getPodDataBuilder(ContainerInterface $container) : PodDataBuilderInterface {
    /** @var \Drupal\ekan_core\Service\PodDataBuilderInterface $builder */
    $builder = $container->get('ekan_core.dcatv11_data_builder');
    return $builder;
  }

  /**
   * Retrieve a single DCAT xml record for a dataset.
   */
  public function retrieveDataset(EkanDatasetEntity $dataset) {
    $encoder = new XmlEncoder([XmlEncoder::ROOT_NODE_NAME => 'rdf:RDF']);
    $normalizer = new GetSetMethodNormalizer();
    $serializer = new Serializer([$normalizer], [$encoder]);
    $build = $this->datasetBuilder->build([$dataset->id()]);
    unset($build['dcat:Catalog']);
    return new Response($serializer->serialize($build, 'xml'), 200, ['Content-Type' => 'application/xml']);
  }

}
