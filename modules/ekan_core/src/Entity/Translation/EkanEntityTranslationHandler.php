<?php

namespace Drupal\ekan_core\Entity\Translation;

use Drupal\content_translation\ContentTranslationHandler;
use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Translation handler for datasets and resources.
 */
class EkanEntityTranslationHandler extends ContentTranslationHandler {

  /**
   * {@inheritDoc}
   */
  public function entityFormAlter(array &$form, FormStateInterface $form_state, EntityInterface $entity) : void {
    parent::entityFormAlter($form, $form_state, $entity);
    $form_display = $form_state->getStorage()['form_display'] ?? NULL;
    if ($form_display instanceof EntityFormDisplayInterface) {
      $translation_weight = $form_display->getComponent('translation')['weight'] ?? NULL;
      if ($translation_weight) {
        $form['content_translation']['#weight'] = $translation_weight;
      }
    }
  }

}
