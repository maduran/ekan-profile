<?php

namespace Drupal\ekan_core\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Url;
use Drupal\ekan_core\Entity\EkanDatasetEntity;

/**
 * Plugin implementation of the 'group_search_link' formatter.
 *
 * @FieldFormatter(
 *   id = "license_formatter",
 *   label = @Translation("License Formatter"),
 *   field_types = {
 *     "list_string"
 *   }
 * )
 */
class LicenseFormatter extends FormatterBase {

  /**
   * {@inheritDoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $licence = $item->value;
      $info = EkanDatasetEntity::licenceHandler()->getLicenseOptions()[$licence] ?? NULL;
      $label = $info['label'] ?? $licence;
      $uri = $info['uri'] ?? NULL;

      $elements[$delta]['value'] = [
        '#type' => 'link',
        '#title' => [
          '#type' => 'inline_template',
          '#template' => '{{ value|nl2br }}',
          '#context' => ['value' => $label],
        ],
        '#url' => $uri ? Url::fromUri($uri) : Url::fromRoute('<nolink>'),
      ];

      if ($uri) {
        $elements[$delta]['od_link'] = [
          '#type' => 'inline_template',
          '#template' => '<a href="{{ uri }}"><img class="open-data od-link" src="https://assets.okfn.org/images/ok_buttons/od_80x15_blue.png" alt="[Open Data]"></a>',
          '#context' => ['uri' => $uri],
        ];
      }
    }

    return $elements;
  }

  /**
   * {@inheritDoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getTargetEntityTypeId() == 'dataset' && $field_definition->getName() == 'license';
  }

}
