<?php

namespace Drupal\ekan_core\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Render\Markup;
use Drupal\ekan_core\Entity\EkanDatasetEntity;

/**
 * Provides a 'other_access_api_links' block.
 *
 * This is a port of the API Access block from DKAN.
 *
 * @Block(
 *  id = "other_access_api_links",
 *  admin_label = @Translation("Other Access API Links"),
 *  context_definitions = {
 *    "dataset" = @ContextDefinition(
 *       "entity:dataset",
 *       label = @Translation("Dataset"),
 *       required = TRUE
 *     )
 *  }
 * )
 */
class OtherAccessApiLinks extends BlockBase {

  /**
   * {@inheritDoc}
   */
  public function build() : array {

    $build = [];

    $dataset = $this->getContextValue('dataset');

    if (!($dataset instanceof EkanDatasetEntity)) {
      return $build;
    }

    CacheableMetadata::createFromObject($dataset)->applyTo($build);

    try {
      $dcat_url = $dataset->toUrl('dcat');
    }
    catch (\Exception $ex) {
      return $build;
    }

    $build += [
      'formats' => [
        '#type' => 'container',
        '#markup' => t('The information on this page (the dataset metadata) is also available in these formats.'),
      ],
      'buttons' => [
        '#type' => 'container',
        '#attributes' => ['class' => ['btn-group']],
        'dcat-xml' => [
          '#type' => 'link',
          '#title' => [
            '#markup' => Markup::create("<i class='ckan-icon ckan-icon-json'></i> RDF"),
          ],
          '#url' => $dcat_url,
          '#attributes' => [
            'class' => ['btn', 'btn-secondary', 'btn-sm'],
          ],
        ],
      ],
    ];

    return $build;
  }

}
